
package finitestatemachine;


/**
 * This class is an end state and can be reused by multiple state machines to end themselves
 *
 * @author Ramm4eg
 */
public class EndState extends State {

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#isEnd()
   */
  @Override
  public boolean isEnd() {
    return true;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    // do nothing... all is over
    return null;
  }

}
