
package finitestatemachine;

import engine.GameEngine;

/**
 * This class implements a specific kind of states. These states just wait a specified time and then
 * transition to the next state. The waiting time can be specified via the variable timerInit (you
 * can set it in the constructor of the state). The next state must be specified in the
 * implementation of the abstract method timerExpired().
 *
 * @author Ramm4eg
 */
public abstract class TimerState extends State {
  protected long timer;
  protected long timerInit;

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    timer -= GameEngine.deltaTime;
    // System.out.println("" + timer);
    if (timer <= 0) {
      return timerExpired();
    }
    return null;
  }

  /**
   * Do all the stuff that must be done after the timer has expired. Specify the next state, to
   * which the transition must be done.
   * 
   * @return next state
   */
  protected abstract State timerExpired();

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#onEnter()
   */
  @Override
  protected void onEnter() {
    timer = timerInit;
  }
}
