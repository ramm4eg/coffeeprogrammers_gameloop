
package finitestatemachine;



/**
 * This class specifies a state in a finite state machine. Additionally it contains a static methods
 * that simplify the work with states (and actually are methods of finite state machines, rather
 * than states)
 *
 * @author Ramm4eg
 */
public abstract class State {

  /**
   * This method is called when the state is the currently active one.
   * 
   * @return next state (if same state or null - a transition should not occur)
   */
  abstract protected State update();

  /**
   * This method is called, when a transition out of this state occurs
   */
  protected void onExit() {
    // per default do nothing
  }

  /**
   * This method is called, when a transition into this state occurs
   */
  protected void onEnter() {
    // per default do nothing
  }

  /**
   * @return true, if this state is an end state; false otherwise
   */
  public boolean isEnd() {
    return false; // per default - not end state
  }

  /**
   * Update the provided state and perform a transition to the next state, if needed.
   * 
   * @param substate state to update
   * @return next state (or same state, if no transition happened)
   */
  public static State updateSubstate(State substate) {
    State newState = substate.update();
    if (newState != null && newState != substate) {
      substate.onExit();
      newState.onEnter();
      return newState;
    }
    return substate;
  }

  /**
   * Initialize state. This method is meant to be called on the start states of state machines.
   * 
   * @param state state to initialize
   */
  public static void init(State state) {
    state.onEnter();
  }
}
