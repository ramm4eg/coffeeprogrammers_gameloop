
package coffeesystem;

/**
 * This class keeps track of the ordered, paid coffee and dispensed coffee. It makes sure, that no
 * coffee is dispensed, if it wasn't paid for.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
class OrdersCounter {

  private int orderedEspresso = 0;
  private int orderedLatteMacchiato = 0;
  private int orderedCappuccino = 0;

  /**
   * An espresso was ordered and paid for.
   */
  public void espressoOrdered() {
    orderedEspresso++;
  }

  /**
   * A latte macchiato was ordered and paid for.
   */
  public void latteMacchiatoOrdered() {
    orderedLatteMacchiato++;
  }

  /**
   * A cappuccino was ordered and paid for.
   */
  public void cappuccinoOrdered() {
    orderedCappuccino++;
  }

  /**
   * Check if a coffee machine can dispense an espresso. If it does, remove it from the order list.
   * 
   * @return true, if espresso can be dispensed, false otherwise
   */
  private boolean dispensingEspresso() {
    if (orderedEspresso <= 0) {
      System.out.println("Someone is trying to get an espresso that wasn't paid for!");
      return false;
    } else {
      orderedEspresso--;
      return true;
    }
  }

  /**
   * Check if a coffee machine can dispense a latte macchiato. If it does, remove it from the order
   * list.
   * 
   * @return true, if latte macchiato can be dispensed, false otherwise
   */
  private boolean dispensingLatteMacchiato() {
    if (orderedLatteMacchiato <= 0) {
      System.out.println("Someone is trying to get an latte macchiato that wasn't paid for!");
      return false;
    } else {
      orderedLatteMacchiato--;
      return true;
    }
  }

  /**
   * Check if a coffee machine can dispense an cappuccino. If it does, remove it from the order
   * list.
   * 
   * @return true, if cappuccino can be dispensed, false otherwise
   */
  private boolean dispensingCappuccino() {
    if (orderedCappuccino <= 0) {
      System.out.println("Someone is trying to get an cappucino that wasn't paid for!");
      return false;
    } else {
      orderedCappuccino--;
      return true;
    }
  }

  /**
   * Check if a coffee machine can dispense the provided kind of coffee.
   * 
   * @param kind selected kind of coffee
   * @return true, if coffee can be dispensed, false otherwise
   */
  public boolean dispensing(CoffeeKind kind) {
    boolean result = false;
    switch (kind) {
      case Espresso:
        result = dispensingEspresso();
        break;
      case LatteMacchiato:
        result = dispensingLatteMacchiato();
        break;
      case Cappuccino:
        result = dispensingCappuccino();
        break;
      default:
        // can't happen
        break;
    }
    return result;
  }

}
