
package coffeesystem;

import java.util.LinkedList;

import simulation.SimulationConfig;
import engine.GameObject;

/**
 * This class represents a coffee system, that contains multiple money slots, multiple coffee
 * machines and can serve multiple customers simultaneously.
 * 
 * It is an API for customers, that want to buy coffee.
 * 
 * The coffee machines are implemented as components of the coffee system, so they are updated from
 * the update method of the coffee system and not directly from the game loop in GameEngine.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class CoffeeSystem extends GameObject {

  private OrdersCounter ordersCounter;

  private CoffeeMachine[] coffeeMachines;
  private int currentlySelecting;
  private int currentlyPaying;
  private LinkedList<ICoffeeSystemObserver> observers;

  /**
   * Constructor
   */
  public CoffeeSystem() {
    ordersCounter = new OrdersCounter();
    coffeeMachines = new CoffeeMachine[SimulationConfig.numberOfCoffeeMachines];
    for (int i = 0; i < SimulationConfig.numberOfCoffeeMachines; i++) {
      coffeeMachines[i] = new CoffeeMachine(ordersCounter);
    }

    observers = new LinkedList<ICoffeeSystemObserver>();
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#start()
   */
  @Override
  public void start() {
    for (CoffeeMachine coffeeMachine : coffeeMachines) {
      coffeeMachine.start();
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#update()
   */
  @Override
  public void update() {
    for (CoffeeMachine coffeeMachine : coffeeMachines) {
      coffeeMachine.update();
    }
  }

  /**
   * @return coffee kinds available for purchase
   */
  public CoffeeKind[] getAvailableCoffeeKinds() {
    return CoffeeKind.values();
  }

  /**
   * @return all accepted payment methods
   */
  public PaymentKind[] getAvailablePaymentKinds() {
    return PaymentKind.values();
  }

  /**
   * Take payment for the coffee with provided payment kind. This takes time. Additionally, register
   * the paid coffee in the orders counter, so it can later be dispensed by a coffee machine.
   * 
   * @param paymentKind payment kind
   * @param selection coffee kind
   */
  public void takePayment(PaymentKind paymentKind, CoffeeKind selection) {
    for (ICoffeeSystemObserver observer : observers) {
      observer.coffeeSold(paymentKind);
    }

    switch (selection) {
      case Espresso:
        ordersCounter.espressoOrdered();
        break;
      case LatteMacchiato:
        ordersCounter.latteMacchiatoOrdered();
        break;
      case Cappuccino:
        ordersCounter.cappuccinoOrdered();
        break;
      default:
        // can't happen
        break;
    }
  }

  /**
   * Get a slot (possibility) to see the menu and choose a coffee kind. The system only supports a
   * limited amount of customers that look at the menu at the same time. This amount is specified in
   * SimulationConfig.numberOfSelectionSlots.
   */
  public boolean getSelectionSlot() {
    if (currentlySelecting < SimulationConfig.numberOfSelectionSlots) {
      currentlySelecting++;
      return true;
    }
    return false;
  }

  /**
   * Free up a selection slot, giving the next customer a possibility to see the menu.
   */
  public void freeUpSelectionSlot() {
    currentlySelecting--;
  }

  /**
   * Get a slot (possibility) to pay for the chosen coffee. The system only supports a limited
   * amount of customers that pay at the same time. This amount is specified in
   * SimulationConfig.numberOfPaymentSlots.
   */
  public boolean getCashRegister() {
    if (currentlyPaying < SimulationConfig.numberOfPaymentSlots) {
      currentlyPaying++;
      return true;
    }
    return false;
  }

  /**
   * Free up a payment slot, giving the next customer a possibility to pay for their coffee.
   */
  public void freeUpCashRegister() {
    currentlyPaying--;
  }

  /**
   * Get a free coffee machine to get paid coffee from. The system only supports a limited amount of
   * coffee machines, that work simultaneously. This amount is specified in
   * SimulationConfig.numberOfCoffeeMachines.
   */
  public CoffeeMachine getCoffeeMachine() {
    for (CoffeeMachine coffeeMachine : coffeeMachines) {
      if (coffeeMachine.isFree()) {
        coffeeMachine.setFree(false);
        return coffeeMachine;
      }
    }
    return null;
  }

  /**
   * Add observer to the system, which will be notified about its changes
   * 
   * @param observer coffee system observer
   */
  public void addObserver(ICoffeeSystemObserver observer) {
    this.observers.add(observer);
    for (CoffeeMachine coffeeMachine : coffeeMachines) {
      coffeeMachine.addObserver(observer);
    }
  }
}
