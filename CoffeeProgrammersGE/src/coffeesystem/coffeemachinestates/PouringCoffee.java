
package coffeesystem.coffeemachinestates;

import simulation.SimulationConfig;
import coffeesystem.CoffeeMachine;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a coffee machine, in which this machine pours coffee into a cup.
 * It is active for a time that depends on the type of coffee that is poured.
 *
 * @author Ramm4eg
 */
public class PouringCoffee extends TimerState {

  private CoffeeMachine coffeeMachine;

  /**
   * Constructor
   * 
   * @param coffeeMachine the owner of the state
   */
  public PouringCoffee(CoffeeMachine coffeeMachine) {
    this.coffeeMachine = coffeeMachine;
    switch (coffeeMachine.getOrder()) {
      case Espresso:
        timerInit = SimulationConfig.ESPRESSO_POURING_TIME;
        break;
      case LatteMacchiato:
        timerInit = SimulationConfig.LATTE_MACCHIATO_POURING_TIME;
        break;
      case Cappuccino:
        timerInit = SimulationConfig.CAPPUCCINO_POURING_TIME;
        break;
      default:
        // can't happen
        break;
    }
  }


  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    coffeeMachine.coffeeIsDone();
    return new WaitingForOrder(coffeeMachine);
  }
}
