
package coffeesystem.coffeemachinestates;

import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import finitestatemachine.State;

/**
 * This class represents a state of a coffee machine, in which this machine is already occupied by a
 * customer, but she has not yet provided her order.
 *
 * @author Ramm4eg
 */
public class WaitingForOrder extends State {

  private CoffeeMachine coffeeMachine;

  /**
   * Constructor
   * 
   * @param machine owner of the state
   */
  public WaitingForOrder(CoffeeMachine machine) {
    this.coffeeMachine = machine;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    CoffeeKind order = coffeeMachine.getOrder();
    if (order != null) {
      return new PouringCoffee(coffeeMachine);
    }
    return null;
  }

}
