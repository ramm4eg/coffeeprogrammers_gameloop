
package coffeesystem;

import java.util.LinkedList;

import coffeesystem.coffeemachinestates.WaitingForOrder;
import engine.GameObject;
import finitestatemachine.State;

/**
 * This class represents a coffee machine that pours selected beverage into a cup
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class CoffeeMachine extends GameObject {

  private State currentState;
  private CoffeeKind order = null;
  private boolean cupFull;
  private OrdersCounter ordersCounter;
  private boolean free = true;
  private LinkedList<ICoffeeSystemObserver> observers;

  /**
   * Constructor
   * 
   * @param counter orders counter
   */
  public CoffeeMachine(OrdersCounter counter) {
    this.ordersCounter = counter;
    observers = new LinkedList<ICoffeeSystemObserver>();
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#start()
   */
  @Override
  public void start() {
    currentState = new WaitingForOrder(this);
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#update()
   */
  @Override
  public void update() {
    currentState = State.updateSubstate(currentState);
    // run endlessly
  }

  /**
   * Request the coffee machine to pour coffee of the provided kind. It's a trigger for the machine
   * to switch current state to PouringCoffee. Before the coffee is dispensed, the check is done, if
   * the coffee has been paid for. If not - no coffee is dispensed.
   * 
   * @param kind coffee kind to dispense
   */
  public boolean pourCoffee(CoffeeKind selectedCoffeeKind) {
    if (ordersCounter.dispensing(selectedCoffeeKind)) {
      order = selectedCoffeeKind;
      return true;
    }
    return false;
  }

  /**
   * Called by one of the coffee machine states to signalize, that the coffee is dispensed.
   */
  public void coffeeIsDone() {
    for (ICoffeeSystemObserver observer : observers) {
      observer.coffeeDispenced(this, order);
    }
    order = null;
    cupFull = true;
  }

  /**
   * Called by customer, that picks up a coffee cup. Sets the coffee machine free for use by further
   * customers.
   */
  public void coffeePickedUp() {
    cupFull = false;
    free = true;
  }

  /**
   * @return ordered coffee type (may return null)
   */
  public CoffeeKind getOrder() {
    return order;
  }

  /**
   * @return true, if the machine has ended pouring coffee into a cups
   */
  public boolean isCupFull() {
    return cupFull;
  }

  /**
   * @return true, if machine can be used by further customer
   */
  public boolean isFree() {
    return free;
  }

  /**
   * Set machine free/occupied
   * 
   * @param b true, if machine must be free; false, if occupied
   */
  public void setFree(boolean b) {
    free = b;
  }

  /**
   * Add observer to the machine, which will be notified about its changes
   * 
   * @param observer coffee system observer
   */
  public void addObserver(ICoffeeSystemObserver observer) {
    observers.add(observer);
  }

}
