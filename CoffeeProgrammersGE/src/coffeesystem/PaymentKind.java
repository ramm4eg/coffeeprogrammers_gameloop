
package coffeesystem;

/**
 * This enumeration contains all kinds of payment, that exist in this universe
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public enum PaymentKind {
  Credit, Cash;
}
