
package coffeesystem;

/**
 * Observer, that can be provided to a coffee system. It will then be notified, when interesting
 * events happen in the system.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public interface ICoffeeSystemObserver {

  /**
   * Notify the observer, that a coffee was payed for
   * 
   * @param paymentKind payment kind
   */
  void coffeeSold(PaymentKind paymentKind);

  /**
   * Notify the observer, that a coffee was dispensed
   * 
   * @param coffeeMachine coffee machine, which dispensed the coffee
   * @param order kind of dispensed coffee
   */
  void coffeeDispenced(CoffeeMachine coffeeMachine, CoffeeKind order);

}
