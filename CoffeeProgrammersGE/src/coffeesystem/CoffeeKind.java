
package coffeesystem;

/**
 * This enum contains all coffee kinds existing in this universe
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public enum CoffeeKind {
  Espresso, LatteMacchiato, Cappuccino;
}
