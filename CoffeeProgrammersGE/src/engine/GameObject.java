
package engine;

/**
 * This class specifies a Game Object - an object that can be registered in the GameEngine to be
 * regularly updated in the Game Loop.
 *
 * @author Ramm4eg
 */
public abstract class GameObject {

  private boolean active = true;

  /**
   * Called in the initialization phase
   */
  abstract public void start();

  /**
   * Called regularly in each simulation step of the game loop
   */
  abstract public void update();

  /**
   * Is this object still active. Inactive objects are ignored in the Game Loop.
   * 
   * @return true if active, false otherwise
   */
  public boolean isActive() {
    return active;
  }

  /**
   * Activate/deactivate the object
   * 
   * @param active true - activate; false - deactivate
   */
  public void setActive(boolean active) {
    this.active = active;
  }
}
