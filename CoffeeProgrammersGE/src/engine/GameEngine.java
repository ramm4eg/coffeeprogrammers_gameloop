
package engine;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import simulation.SimulationConfig;

/**
 * This class is the implementation of the Game Loop pattern. It's a central class of the simulation
 * - it runs in a loop and calls all registered game objects.
 *
 * @author Ramm4eg
 */
public class GameEngine {

  private static List<GameObject> gameObjects = new LinkedList<GameObject>();
  public static boolean stop = false;
  public static long deltaTime = 0;

  /**
   * Run the game loop.
   */
  public static void run() {

    // initialize all game objects
    for (GameObject obj : gameObjects) {
      obj.start();
    }

    // run the game loop
    Date oldTimeStamp = new Date();
    while (!stop) {
      Date timeStamp = new Date();
      deltaTime = timeStamp.getTime() - oldTimeStamp.getTime();
      oldTimeStamp = timeStamp;

      for (GameObject obj : gameObjects) {
        if (obj.isActive()) {
          obj.update();
        }
      }

      if (!stop) { // no need to wait, if all is done
        Date afterUpdate = new Date();
        long elapsedTime = afterUpdate.getTime() - oldTimeStamp.getTime();
        try {
          // the simulation step must be not quicker than the specified step time
          if (SimulationConfig.SIMULATION_STEP_TIME - elapsedTime >= 0) {
            Thread.sleep(SimulationConfig.SIMULATION_STEP_TIME - elapsedTime);
          }
        } catch (InterruptedException e) {
        }
      }

    }

  }

  /**
   * Register a game object in the game engine. Registered objects will be called in the game loop.
   * 
   * @param gameObject game object
   */
  public static void register(GameObject gameObject) {
    gameObjects.add(gameObject);
  }

  public static void reset() {
    stop = false;
    gameObjects.clear();
  }
}
