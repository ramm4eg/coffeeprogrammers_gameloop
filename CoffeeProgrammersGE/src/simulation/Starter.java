
package simulation;

import java.util.Date;

import programmers.Programmer;
import coffeesystem.CoffeeSystem;
import engine.GameEngine;


/**
 * This class is the starting class of the exercise. It contains the main method, which instantiates
 * the coffee system and all the programmers that want coffee.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class Starter {

  /**
   * Main method for the people, who don't like the SWT GUI and don't need any runtime
   * parameterization.
   * 
   * @param args arguments of this method are ignored
   */
  public static void main(String[] args) {
    System.out.println(simulate());
  }

  /**
   * Instantiate the coffee system and all the programmers that want coffee. Gather the statistics
   * after the simulation and format it into a result text.
   * 
   * @return statistics of the simulation as text
   */
  public static String simulate() {
    // reset game engine
    GameEngine.reset();

    // observer, that gathers statistics and keeps track of simulation
    SimulationObserver observer = new SimulationObserver();

    CoffeeSystem coffeeSystem = new CoffeeSystem();
    GameEngine.register(coffeeSystem);
    coffeeSystem.addObserver(observer);

    int numberOfProgrammers = SimulationConfig.numberOfProgrammers;
    Programmer[] programmers = new Programmer[numberOfProgrammers];


    for (int i = 0; i < numberOfProgrammers; i++) {
      Programmer programmer = new Programmer(coffeeSystem);
      programmers[i] = programmer;
      GameEngine.register(programmer);
      programmer.addObserver(observer);
    }

    Date globalStart = new Date();

    GameEngine.run();

    Date globalEnd = new Date();
    long globalTime = globalEnd.getTime() - globalStart.getTime();

    // print results
    String results = "";

    int coffeeSoldForCredit = observer.getCoffeeSoldForCredit();
    int coffeeSoldForCash = observer.getCoffeeSoldForCash();
    results += "=================================================\n";
    results += "Coffee sold:\n";
    results += "    for credit: " + coffeeSoldForCredit + "\n";
    results += "    for cash: " + coffeeSoldForCash + "\n";
    results += "            total: " + (coffeeSoldForCash + coffeeSoldForCredit) + "\n";

    results += "\n";
    results += "Coffee dispensed:" + "\n";
    int[][] coffeeDispensed = observer.getDispensedCoffee();
    for (int i = 0; i < coffeeDispensed.length; i++) {
      results += "    MACHINE " + i + ":" + "\n";
      results += "        Espresso: " + coffeeDispensed[i][0] + "\n";
      results += "        Latte Macchiato: " + coffeeDispensed[i][1] + "\n";
      results += "        Cappuccino: " + coffeeDispensed[i][2] + "\n";
      results +=
          "            total: "
              + (coffeeDispensed[i][0] + coffeeDispensed[i][1] + coffeeDispensed[i][2]) + "\n";
    }

    results += "\n";
    results += "Time getting coffee including waiting in the queue:" + "\n";
    long fastest = observer.getFastestTime();
    results += "    fastest: " + fastest + " ms" + "\n";
    long slowest = observer.getSlowestTime();
    results += "    slowest: " + slowest + " ms" + "\n";
    long avarage = observer.getAverageTime();
    results += "    average: " + avarage + " ms" + "\n";

    results += "\n";
    results += "Overall simulation time: " + globalTime + " ms" + "\n";

    return results;
  }

  /**
   * Change simulation parameterization.
   * 
   * @param numOfProgrammers number of programmers as string
   * @param numOfSelectionSlots number of coffee kind selection slots as string
   * @param numOfPaymentSlots number of payment slots as string
   * @param numOfCoffeeMachines number of coffee machines as string
   */
  public static void prepareConfig(String numOfProgrammers, String numOfSelectionSlots,
      String numOfPaymentSlots, String numOfCoffeeMachines) {
    SimulationConfig.setNumberOfProgrammers(numOfProgrammers);
    SimulationConfig.setNumberOfSelectionSlots(numOfSelectionSlots);
    SimulationConfig.setNumberOfPaymentSlots(numOfPaymentSlots);
    SimulationConfig.setNumberOfCoffeeMachines(numOfCoffeeMachines);
  }

}
