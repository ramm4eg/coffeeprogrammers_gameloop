
package simulation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import programmers.IProgrammerObserver;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import coffeesystem.ICoffeeSystemObserver;
import coffeesystem.PaymentKind;
import engine.GameEngine;

/**
 * This class implements all the observers, needed to build the resulting statistics. It also counts
 * the programmers, that have left and stops the simulation, when all are gone.
 *
 * @author Ramm4eg
 */
public class SimulationObserver implements ICoffeeSystemObserver, IProgrammerObserver {

  private LinkedList<Long> times;
  private int activeProgrammers;
  private int coffeeSoldForCredit;
  private int coffeeSoldForCash;
  private HashMap<CoffeeMachine, Integer[]> dispensedCoffee;

  /**
   * Constructor
   */
  public SimulationObserver() {
    activeProgrammers = SimulationConfig.numberOfProgrammers;
    times = new LinkedList<Long>();
    coffeeSoldForCredit = 0;
    coffeeSoldForCash = 0;
    dispensedCoffee = new HashMap<CoffeeMachine, Integer[]>();
  }

  /**
   * @return statistics about how many times the credit payment was used on all cash registers
   */
  public int getCoffeeSoldForCredit() {
    return coffeeSoldForCredit;
  }

  /**
   * @return statistics about how many times the cash payment was used on all cash registers
   */
  public int getCoffeeSoldForCash() {
    return coffeeSoldForCash;
  }

  /**
   * Calculate fastest of time of getting coffee
   * 
   * @return fastest coffee getting time
   */
  public long getFastestTime() {
    long min = times.getFirst();
    for (Long time : times) {
      if (time < min) {
        min = time;
      }
    }
    return min;
  }

  /**
   * Calculate slowest of time of getting coffee
   * 
   * @return slowest coffee getting time
   */
  public long getSlowestTime() {
    long max = times.getFirst();
    for (Long time : times) {
      if (time > max) {
        max = time;
      }
    }
    return max;
  }

  /**
   * Calculate average of time of getting coffee
   * 
   * @return average coffee getting time
   */
  public long getAverageTime() {
    long timeTotal = 0;
    for (Long time : times) {
      timeTotal += time;
    }
    return timeTotal / times.size();
  }

  /**
   * Provide statistics about how much coffee the coffee machines have poured. The result is a 2D
   * array, where the first index is the index of a coffee machine and the second index represents a
   * coffee type: 1-Espresso, 2-Latte Macchiato, 3-Cappuccino.
   * 
   * @return statistics about how much coffee was dispensed
   */
  public int[][] getDispensedCoffee() {
    Set<CoffeeMachine> coffeeMachines = dispensedCoffee.keySet();
    int[][] result = new int[coffeeMachines.size()][3];

    int index = 0;
    for (CoffeeMachine coffeeMachine : coffeeMachines) {
      Integer[] coffeeKinds = dispensedCoffee.get(coffeeMachine);
      result[index][0] = coffeeKinds[0];
      result[index][1] = coffeeKinds[1];
      result[index][2] = coffeeKinds[2];
      index++;
    }
    return result;
  }

  /**
   * {@inheritDoc}
   * 
   * @see programmers.IProgrammerObserver#programmerLeft(long)
   */
  @Override
  public void programmerLeft(long usedTime) {
    times.add(usedTime);
    activeProgrammers--;

    if (activeProgrammers <= 0) {
      GameEngine.stop = true;
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see coffeesystem.ICoffeeSystemObserver#coffeeSold(coffeesystem.PaymentKind)
   */
  @Override
  public void coffeeSold(PaymentKind paymentKind) {
    switch (paymentKind) {
      case Credit:
        coffeeSoldForCredit++;
        break;
      case Cash:
        coffeeSoldForCash++;
        break;
      default:
        // can't happen
        break;
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see coffeesystem.ICoffeeSystemObserver#coffeeDispenced(coffeesystem.CoffeeMachine,
   *      coffeesystem.CoffeeKind)
   */
  @Override
  public void coffeeDispenced(CoffeeMachine coffeeMachine, CoffeeKind order) {
    Integer[] dispensedCoffeeKinds;
    if (dispensedCoffee.containsKey(coffeeMachine)) {
      dispensedCoffeeKinds = dispensedCoffee.get(coffeeMachine);
    } else {
      dispensedCoffeeKinds = new Integer[] {0, 0, 0};
      dispensedCoffee.put(coffeeMachine, dispensedCoffeeKinds);
    }

    switch (order) {
      case Espresso:
        dispensedCoffeeKinds[0]++;
        break;
      case LatteMacchiato:
        dispensedCoffeeKinds[1]++;
        break;
      case Cappuccino:
        dispensedCoffeeKinds[2]++;
        break;

      default:
        // can't happen
        break;
    }
  }
}
