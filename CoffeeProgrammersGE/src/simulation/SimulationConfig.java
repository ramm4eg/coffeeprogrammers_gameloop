/**
 * SimulationConfig.java
 */

package simulation;

/**
 * This class contains all the constants and parameters of the coffee system simulation
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class SimulationConfig {

  /*
   * Time Constants
   */
  public static final long COFFEE_CHOOSING_TIME = 500; // ms
  public static final long CREDIT_PAYMENT_TIME = 250; // ms
  public static final long CASH_PAYMENT_TIME = 500; // ms
  public static final long CUP_FINDING_TIME = 250; // ms
  public static final long CUP_UNDER_OUTLET_PUTTING_TIME = 250; // ms
  public static final long COFFEE_TYPE_PICKING_TIME = 250; // ms
  public static final long ESPRESSO_POURING_TIME = 250; // ms
  public static final long LATTE_MACCHIATO_POURING_TIME = 500; // ms
  public static final long CAPPUCCINO_POURING_TIME = 750; // ms
  public static final long LEAVING_TIME = 250; // ms
  public static final long SIMULATION_STEP_TIME = 10; // ms

  /*
   * Simulation parameters
   */
  public static int numberOfProgrammers = 100;
  public static int numberOfSelectionSlots = 10;
  public static int numberOfPaymentSlots = 5;
  public static int numberOfCoffeeMachines = 2;

  /**
   * Provide a simulation parameter as string to support parameterizing the simulation from GUI
   * 
   * @param numOfProgrammers number of programmers as string
   */
  public static void setNumberOfProgrammers(String numOfProgrammers) {
    numberOfProgrammers = Integer.parseInt(numOfProgrammers);
  }

  /**
   * Provide a simulation parameter as string to support parameterizing the simulation from GUI
   * 
   * @param numOfSelectionSlots how many people can read the menu and choose coffee simultaneously
   */
  public static void setNumberOfSelectionSlots(String numOfSelectionSlots) {
    numberOfSelectionSlots = Integer.parseInt(numOfSelectionSlots);
  }

  /**
   * Provide a simulation parameter as string to support parameterizing the simulation from GUI
   * 
   * @param numOfPaymentSlots how many people can pay for their coffee simultaneously
   */
  public static void setNumberOfPaymentSlots(String numOfPaymentSlots) {
    numberOfPaymentSlots = Integer.parseInt(numOfPaymentSlots);
  }

  /**
   * Provide a simulation parameter as string to support parameterizing the simulation from GUI
   * 
   * @param numOfCoffeeMachines how many people can get their coffee simultaneously
   */
  public static void setNumberOfCoffeeMachines(String numOfCoffeeMachines) {
    numberOfCoffeeMachines = Integer.parseInt(numOfCoffeeMachines);
  }

}
