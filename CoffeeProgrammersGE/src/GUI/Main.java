
package GUI;


/**
 * The main starter class
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class Main {

  /**
   * @param args arguments are ignored
   */
  public static void main(String[] args) {
    GUI gui = new GUI();
    gui.open();
  }

}
