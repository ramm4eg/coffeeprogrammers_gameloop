
package GUI;

import simulation.Starter;

/**
 * This class is an additional thread for execution of the simulation from GUI. It frees up the GUI
 * thread.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class SimulationThread extends Thread {


  private String resultString;

  /**
   * {@inheritDoc}
   * 
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    resultString = Starter.simulate();
  }

  /**
   * @return simulation results
   */
  public String getResult() {
    return resultString;
  }
}
