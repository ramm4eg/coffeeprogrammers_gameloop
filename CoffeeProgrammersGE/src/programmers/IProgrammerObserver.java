
package programmers;

/**
 * Observer, that can be provided to a programmer. It will then be notified, when interesting events
 * happen with the programmer.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public interface IProgrammerObserver {

  /**
   * Notify the observer, that a programmer has got her coffee and left
   * 
   * @param usedTime time used by the programmer to get coffee
   */
  void programmerLeft(long usedTime);

}
