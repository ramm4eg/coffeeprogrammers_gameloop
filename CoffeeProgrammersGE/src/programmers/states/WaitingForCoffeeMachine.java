
package programmers.states;

import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import coffeesystem.CoffeeSystem;
import finitestatemachine.State;


/**
 * This class represents a state of a programmer, in which this programmer waits for a free coffee
 * machine. It is active until a coffee machine is freed up and the programmer can get her coffee.
 * Next state is GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class WaitingForCoffeeMachine extends State {

  private CoffeeSystem coffeeSystem;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param selectedCoffeeKind selected coffee kind (needed for future states)
   * @param coffeeSystem system to buy coffee from
   */
  public WaitingForCoffeeMachine(CoffeeKind selectedCoffeeKind, CoffeeSystem coffeeSystem) {
    this.selectedCoffeeKind = selectedCoffeeKind;
    this.coffeeSystem = coffeeSystem;
  }

  @Override
  protected State update() {
    CoffeeMachine coffeeMachine = coffeeSystem.getCoffeeMachine();
    if (coffeeMachine != null) {
      return new GettingCoffee(coffeeMachine, selectedCoffeeKind);
    }
    return null;
  }

}
