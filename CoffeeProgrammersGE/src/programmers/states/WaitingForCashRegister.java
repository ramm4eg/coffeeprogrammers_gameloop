
package programmers.states;

import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeSystem;
import finitestatemachine.State;


/**
 * This class represents a state of a programmer, in which this programmer waits for a free cash
 * register. It is active until a cash register is freed up and the programmer can pay for her
 * coffee. Next state is PayingForCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class WaitingForCashRegister extends State {

  private CoffeeSystem coffeeSystem;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param selection selected coffee kind (needed for future states)
   * @param coffeeSystem system to buy coffee from
   */
  public WaitingForCashRegister(CoffeeKind selection, CoffeeSystem coffeeSystem) {
    this.selectedCoffeeKind = selection;
    this.coffeeSystem = coffeeSystem;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    if (coffeeSystem.getCashRegister()) {
      return new PayingForCoffee(selectedCoffeeKind, coffeeSystem);
    }
    return null;
  }

}
