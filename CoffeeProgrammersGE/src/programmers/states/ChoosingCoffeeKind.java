
package programmers.states;

import simulation.SimulationConfig;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeSystem;
import finitestatemachine.State;
import finitestatemachine.TimerState;


/**
 * This class represents a state of a programmer, in which this programmer chooses her coffee. It is
 * active for a time that is specified in SimulationConfig.COFFEE_CHOOSING_TIME. When the time
 * expires, next state is calculated. If there is no free cash register, the next state is
 * WaitingForCashRegister. If there is one, the state changes directly to PayingForCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class ChoosingCoffeeKind extends TimerState {

  private CoffeeSystem coffeeSystem;

  /**
   * Constructor
   * 
   * @param coffeeSystem system to buy coffee from
   */
  public ChoosingCoffeeKind(CoffeeSystem coffeeSystem) {
    timerInit = SimulationConfig.COFFEE_CHOOSING_TIME;
    this.coffeeSystem = coffeeSystem;
  }

  /**
   * Choose the favorite (well, random) coffee kind from the offered ones.
   * 
   * @param availableCoffeeKinds offered kinds of coffee
   * @return favorite coffee kind
   */
  private CoffeeKind chooseCoffeeKind(CoffeeKind[] availableCoffeeKinds) {
    int selectionIndex = (int) (Math.random() * (availableCoffeeKinds.length));
    return availableCoffeeKinds[selectionIndex];
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    CoffeeKind[] availableCoffeeKinds = coffeeSystem.getAvailableCoffeeKinds();
    CoffeeKind selection = chooseCoffeeKind(availableCoffeeKinds);

    if (coffeeSystem.getCashRegister()) {
      return new PayingForCoffee(selection, coffeeSystem);
    } else {
      return new WaitingForCashRegister(selection, coffeeSystem);
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#onExit()
   */
  @Override
  protected void onExit() {
    coffeeSystem.freeUpSelectionSlot();
  }
}
