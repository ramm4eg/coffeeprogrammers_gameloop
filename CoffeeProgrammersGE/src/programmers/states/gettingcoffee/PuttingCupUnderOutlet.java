
package programmers.states.gettingcoffee;

import simulation.SimulationConfig;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a programmer, in which this programmer puts a cup into coffee
 * machine. It is active for a time that is specified in
 * SimulationConfig.CUP_UNDER_OUTLET_PUTTING_TIME. When the time expires, next state is
 * PickingCoffeeType.
 * 
 * This state is a substate of GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class PuttingCupUnderOutlet extends TimerState {

  private CoffeeMachine coffeeMachine;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine (needed for future states)
   * @param selectedCoffeeKind coffee kind (needed for future states)
   */
  public PuttingCupUnderOutlet(CoffeeMachine coffeeMachine, CoffeeKind selectedCoffeeKind) {
    timerInit = SimulationConfig.CUP_UNDER_OUTLET_PUTTING_TIME;
    this.coffeeMachine = coffeeMachine;
    this.selectedCoffeeKind = selectedCoffeeKind;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    return new PickingCoffeeType(coffeeMachine, selectedCoffeeKind);
  }

}
