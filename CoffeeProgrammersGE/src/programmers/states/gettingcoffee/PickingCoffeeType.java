
package programmers.states.gettingcoffee;

import simulation.SimulationConfig;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import finitestatemachine.EndState;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a programmer, in which this programmer selects the type of
 * coffee, he paid for, in the coffee machine. It is active for a time that is specified in
 * SimulationConfig.COFFEE_TYPE_PICKING_TIME. When the time expires, next state is WaitingForCoffee.
 * 
 * This state is a substate of GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class PickingCoffeeType extends TimerState {

  private CoffeeMachine coffeeMachine;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine (needed for future states)
   * @param selectedCoffeeKind coffee kind (needed for future states)
   */
  public PickingCoffeeType(CoffeeMachine coffeeMachine, CoffeeKind selectedCoffeeKind) {
    this.coffeeMachine = coffeeMachine;
    this.selectedCoffeeKind = selectedCoffeeKind;
    timerInit = SimulationConfig.COFFEE_TYPE_PICKING_TIME;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    if (coffeeMachine.pourCoffee(selectedCoffeeKind)) {
      return new WaitingForCoffee(coffeeMachine);
    } else {
      System.out.println("Something went wrong - a programmer leaves without coffee.");
      return new EndState();
    }
  }
}
