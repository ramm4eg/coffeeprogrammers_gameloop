
package programmers.states.gettingcoffee;

import simulation.SimulationConfig;
import coffeesystem.CoffeeMachine;
import finitestatemachine.EndState;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a programmer, in which this programmer picks up her coffee and
 * leaves. It is active for a time that is specified in SimulationConfig.LEAVING_TIME. When the time
 * expires, next state is EndState.
 * 
 * This state is a substate of GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class PickUpCoffeeAndLeave extends TimerState {

  private CoffeeMachine coffeeMachine;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine to get coffee from
   */
  public PickUpCoffeeAndLeave(CoffeeMachine coffeeMachine) {
    this.coffeeMachine = coffeeMachine;
    timerInit = SimulationConfig.LEAVING_TIME;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    // pick up coffee
    coffeeMachine.coffeePickedUp();
    // and leave
    return new EndState();
  }

}
