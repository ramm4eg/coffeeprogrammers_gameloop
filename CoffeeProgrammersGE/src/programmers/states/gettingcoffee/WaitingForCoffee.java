
package programmers.states.gettingcoffee;

import coffeesystem.CoffeeMachine;
import finitestatemachine.State;

/**
 * This class represents a state of a programmer, in which this programmer waits for a coffee
 * machine to pour his coffee into a cup. It is active until the cup is full. Next state is
 * PickUpCoffeeAndLeave.
 * 
 * This state is a substate of GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class WaitingForCoffee extends State {

  private CoffeeMachine coffeeMachine;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine to get coffee from
   */
  public WaitingForCoffee(CoffeeMachine coffeeMachine) {
    this.coffeeMachine = coffeeMachine;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    if (coffeeMachine.isCupFull()) {
      return new PickUpCoffeeAndLeave(coffeeMachine);
    }
    return null;
  }

}
