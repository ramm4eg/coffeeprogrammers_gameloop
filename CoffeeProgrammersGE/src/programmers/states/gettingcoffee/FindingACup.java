
package programmers.states.gettingcoffee;

import simulation.SimulationConfig;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a programmer, in which this programmer looks for a cup to put
 * into coffee machine. It is active for a time that is specified in
 * SimulationConfig.CUP_FINDING_TIME. When the time expires, next state is PuttingCupUnderOutler.
 * 
 * This state is a substate of GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class FindingACup extends TimerState {

  private CoffeeMachine coffeeMachine;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine (needed for future states)
   * @param selectedCoffeeKind coffee kind (needed for future states)
   */
  public FindingACup(CoffeeMachine coffeeMachine, CoffeeKind selectedCoffeeKind) {
    timerInit = SimulationConfig.CUP_FINDING_TIME;
    this.coffeeMachine = coffeeMachine;
    this.selectedCoffeeKind = selectedCoffeeKind;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    return new PuttingCupUnderOutlet(coffeeMachine, selectedCoffeeKind);
  }

}
