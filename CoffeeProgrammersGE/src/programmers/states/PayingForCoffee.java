
package programmers.states;

import simulation.SimulationConfig;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import coffeesystem.CoffeeSystem;
import coffeesystem.PaymentKind;
import finitestatemachine.State;
import finitestatemachine.TimerState;

/**
 * This class represents a state of a programmer, in which this programmer pays for her coffee. It
 * is active for a time that depends on the payment kind - SimulationConfig.CREDIT_PAYMENT_TIME or
 * SimulationConfig.CASH_PAYMENT_TIME. When the time expires, next state is calculated. If there is
 * no free coffee machine, the next state is WaitingForCoffeeMachine. If there is one, the state
 * changes directly to GettingCoffee.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class PayingForCoffee extends TimerState {

  private CoffeeSystem coffeeSystem;
  private CoffeeKind selectedCoffeeKind;
  private PaymentKind paymentKind;

  /**
   * Constructor
   * 
   * @param selection selected coffee kind
   * @param coffeeSystem system to buy coffee from
   */
  public PayingForCoffee(CoffeeKind selection, CoffeeSystem coffeeSystem) {
    this.selectedCoffeeKind = selection;
    this.coffeeSystem = coffeeSystem;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#onEnter()
   */
  @Override
  protected void onEnter() {
    PaymentKind[] availablePaymentKinds = coffeeSystem.getAvailablePaymentKinds();
    paymentKind = choosePaymentKind(availablePaymentKinds);

    switch (paymentKind) {
      case Credit:
        timer = SimulationConfig.CREDIT_PAYMENT_TIME;
        break;
      case Cash:
        timer = SimulationConfig.CASH_PAYMENT_TIME;
      default:
        // can't happen
        break;
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#onExit()
   */
  @Override
  protected void onExit() {
    coffeeSystem.freeUpCashRegister();
  }

  /**
   * Choose the favorite (well, also random) payment method from the provided ones
   * 
   * @param availablePaymentKinds all accepted payment kinds
   * 
   * @return favorite payment kind
   */
  private PaymentKind choosePaymentKind(PaymentKind[] availablePaymentKinds) {
    int paymentKindIndex = (int) (Math.random() * (availablePaymentKinds.length));
    PaymentKind paymentKind = availablePaymentKinds[paymentKindIndex];
    return paymentKind;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.TimerState#timerExpired()
   */
  @Override
  protected State timerExpired() {
    coffeeSystem.takePayment(paymentKind, selectedCoffeeKind);
    CoffeeMachine machine = coffeeSystem.getCoffeeMachine();
    if (machine != null) {
      return new GettingCoffee(machine, selectedCoffeeKind);
    } else {
      return new WaitingForCoffeeMachine(selectedCoffeeKind, coffeeSystem);
    }
  }
}
