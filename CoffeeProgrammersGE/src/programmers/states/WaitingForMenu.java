
package programmers.states;

import coffeesystem.CoffeeSystem;
import finitestatemachine.State;

/**
 * This class represents a state of a programmer, in which this programmer waits for a place in
 * front of the menu. It is active until such place is freed up and the programmer can choose her
 * favorite coffee. Next state is ChoosingCoffeeKind.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class WaitingForMenu extends State {

  CoffeeSystem coffeeSystem;

  /**
   * Constructor
   * 
   * @param coffeeSystem system to buy coffee from
   */
  public WaitingForMenu(CoffeeSystem coffeeSystem) {
    this.coffeeSystem = coffeeSystem;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    if (coffeeSystem.getSelectionSlot()) {
      return new ChoosingCoffeeKind(coffeeSystem);
    }
    return null;
  }

}
