
package programmers.states;

import programmers.states.gettingcoffee.FindingACup;
import coffeesystem.CoffeeKind;
import coffeesystem.CoffeeMachine;
import finitestatemachine.EndState;
import finitestatemachine.State;

/**
 * This class represents a state of a programmer, in which this programmer gets her coffee. It
 * consists of multiple substates. When all substates are exited (the current substate is an end
 * state), the transition to the next state occurs. Next state is EndState.
 *
 * @author Mykhaylo Shlyakhtovskyy
 */
public class GettingCoffee extends State {

  private State substate;
  private CoffeeMachine coffeeMachine;
  private CoffeeKind selectedCoffeeKind;

  /**
   * Constructor
   * 
   * @param coffeeMachine coffee machine that will dispense the coffee
   * @param selectedCoffeeKind coffee to get
   */
  public GettingCoffee(CoffeeMachine coffeeMachine, CoffeeKind selectedCoffeeKind) {
    this.coffeeMachine = coffeeMachine;
    this.selectedCoffeeKind = selectedCoffeeKind;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#update()
   */
  @Override
  protected State update() {
    substate = updateSubstate(substate);
    if (substate.isEnd()) {
      return new EndState();
    }
    return null;
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#onEnter()
   */
  @Override
  protected void onEnter() {
    substate = new FindingACup(coffeeMachine, selectedCoffeeKind);
    State.init(substate);
    update();
  }

  /**
   * {@inheritDoc}
   * 
   * @see finitestatemachine.State#onExit()
   */
  @Override
  protected void onExit() {
    substate = null;
  }

}
