
package programmers;

import java.util.Date;
import java.util.LinkedList;

import programmers.states.ChoosingCoffeeKind;
import programmers.states.WaitingForMenu;
import coffeesystem.CoffeeSystem;
import engine.GameObject;
import finitestatemachine.State;


/**
 * This class represents a programmer that wants some coffee. Programmer is a game object, that is
 * updated in the game loop in GameEngine.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class Programmer extends GameObject {


  private Date start;
  private State currentState;
  private long usedTime = 0;
  private CoffeeSystem coffeeSystem;
  private LinkedList<IProgrammerObserver> observers;

  /**
   * Constructor
   * 
   * @param coffeeSystem coffee system to get coffee from
   */
  public Programmer(CoffeeSystem coffeeSystem) {
    this.coffeeSystem = coffeeSystem;
    observers = new LinkedList<IProgrammerObserver>();
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#start()
   */
  @Override
  public void start() {
    start = new Date();

    if (coffeeSystem.getSelectionSlot()) {
      // start choosing coffee already in the first step
      currentState = new ChoosingCoffeeKind(coffeeSystem);
      State.init(currentState);
    } else {
      currentState = new WaitingForMenu(coffeeSystem);
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @see engine.GameObject#update()
   */
  @Override
  public void update() {
    currentState = State.updateSubstate(currentState);

    if (currentState.isEnd()) {
      Date end = new Date();
      usedTime = end.getTime() - start.getTime();
      setActive(false);

      for (IProgrammerObserver observer : observers) {
        observer.programmerLeft(usedTime);
      }
    }
  }

  /**
   * Add observer to the programmer, which will be notified about its changes
   * 
   * @param observer programmer observer
   */
  public void addObserver(IProgrammerObserver observer) {
    observers.add(observer);
  }

}
